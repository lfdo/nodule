<!--
SPDX-License-Identifier: Apache-2.0
SPDX-FileCopyrightText: Copyright 2022 David Seaward and contributors
-->

# Nodule roadmap (don't hold your breath)

## Minimal install

- Minimal installation by default
- Optional:
    - nodule\[yaml\]
    - nodule\[full\] == yaml + CLI
